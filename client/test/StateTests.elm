
module StateTests exposing (all)

import           ElmTest exposing (..)
import           List exposing (..)
import           Set exposing (..)

import           Configuration exposing (..)
import           KeyboardState exposing (KeyboardEvent(..))
import           Position exposing (Delta(..), Level(..), init)
import           State exposing (..)

all : Test
all = suite "StateTests" <|
  bothPressed ::
  positionWrapping ::
  obstaclesDescend ::
  removeObstacles ::
  levelUp ::
  levelDown ::
  noLevelLimit ::
  phase ::
  cheat ::
  []

testCfg : Configuration
testCfg = {
    gameHeight = 100,
    gameWidth = 100,
    obstacleVelocity = 1,
    padding = 3,
    playerSize = 16,
    levelUpVelocity = 0.3,
    levelDownVelocity = 0.4,
    playerInfluences = [1, 1],
    transitionFunction = \ x -> x
  }

bothPressed : Test
bothPressed =
  test "both pressed behaves like none pressed" <|
    let
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State (fromList [37, 39]) (Level 0) Position.init [] Playing)
    in assertEqual {values = [0, 0]} result.position

positionWrapping : Test
positionWrapping =
  test "position wraps around" <|
    let
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State (fromList [37, 39]) (Level 2.5) {values = [56, 0]} [] Playing)
    in assertEqual {values = [-44, 0]} result.position

obstaclesDescend : Test
obstaclesDescend =
  test "obstacles in the scene descend" <|
    let
      posA = 10
      obstacle = Obstacle posA (23, 42)
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State empty (Level 0) Position.init [obstacle] Playing)
      expected = Just <| Obstacle (posA - testCfg.obstacleVelocity * 0.5) (23, 42)
    in assertEqual expected (head result.obstacles)

removeObstacles : Test
removeObstacles =
  test "obstacles below the scene are removed" <|
    let
      a = Obstacle -42 (23, 42)
      b = Obstacle -52 (23, 42)
      c = Obstacle -70 (23, 42)
      result = fst <| update testCfg GCObstacles
        (State empty (Level 0) Position.init [a, b, c] Playing)
      expected = [a, b]
    in assertEqual expected result.obstacles

levelUp : Test
levelUp =
  test "will increase the level when there's no collision" <|
    let
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State empty (Level 0) Position.init [] Playing)
      expected = Level 0.15
    in assertEqual expected result.level

levelDown : Test
levelDown =
  test "will increase the level when there's no collision" <|
    let
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State empty (Level 1) Position.init [Obstacle -50 (-10, 10)] Playing)
      expected = Level 0.8
    in assertEqual expected result.level

noLevelLimit : Test
noLevelLimit =
  test "will fall below zero" <|
    let
      result = fst <| update testCfg (Tick (Delta 0.5))
        (State empty (Level 0.1) Position.init [Obstacle -50 (-10, 10)] Playing)
      expected = Level -0.1
    in assertEqual expected result.level

phase : Test
phase = suite "phase" <|
  let
    testPhase : Float -> Phase -> Phase
    testPhase level old =
      update testCfg (Tick (Delta 0.5))
        (State empty (Level level) Position.init [] old)
        |> fst |> .gamePhase
  in
  (test "stays in Playing if in range" <|
    assertEqual Playing (testPhase 0.1 Playing)) ::
  (test "switches to GameOver if the level is below zero" <|
    assertEqual GameOver (testPhase -0.1 Playing)) ::
  (test "switches to Won if the level is above the limit" <|
    assertEqual Won (testPhase (maxLevel testCfg + 0.1) Playing)) ::
  (test "once Won doesn't switch back" <|
    assertEqual Won (testPhase 0.1 Won)) ::
  (test "once GameOver doesn't switch back" <|
    assertEqual GameOver (testPhase 0.1 GameOver)) ::
  []

cheat : Test
cheat = suite "cheat" <|
  (test "+ 42" <|
    let
      result = fst <| update testCfg (Cheat (\ (Level x) -> Level (x + 0.42)))
        (State empty (Level 0.23) Position.init [] Playing)
      expected = Level 0.65
    in assertEqual expected result.level) ::
  []
