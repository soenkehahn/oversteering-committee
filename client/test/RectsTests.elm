
module RectsTests exposing (all, testCfg)

import           Color exposing (..)
import           ElmTest exposing (..)
import           Set

import           Configuration exposing (..)
import           Position exposing (..)
import           Rects exposing (..)
import           State exposing (State, Phase(..), Obstacle(..), rectsPlayer, rectObstacle)

all : Test
all = ElmTest.suite "RectsTests" <|
  player ::
  obstacle ::
  collision ::
  moveRectTest ::
  []

testCfg : Configuration
testCfg = {
    gameHeight = 100,
    gameWidth = 100,
    obstacleVelocity = 1,
    padding = 3,
    playerSize = 16,
    levelUpVelocity = 0.3,
    levelDownVelocity = 0.4,
    playerInfluences = [1, 1],
    transitionFunction = \ x -> x
  }

player : Test
player =
  suite "player" <|
    (test "Player to Rect" <|
      let
        result = rectsPlayer testCfg (State Set.empty (Level 0) init [] Playing)
        expected = [Rect {x = -8, y = -47} {x = 8, y = -31} white]
      in assertEqual expected result) ::
    (test "Player to Rect creates two rects on the wrapping edge" <|
      let
        pos = {values = [46, 0]}
        result = rectsPlayer testCfg (State Set.empty (Level 0) pos [] Playing)
        expected =
          Rect {x = 38, y = -47} {x = 54, y = -31} white ::
          Rect {x = -62, y = -47} {x = -46, y = -31} white ::
          []
      in assertEqual expected result) ::
    (test "Player to Rect creates two rects on the wrapping edge other side" <|
      let
        pos = {values = [-46, 0]}
        result = rectsPlayer testCfg (State Set.empty (Level 0) pos [] Playing)
        expected =
          Rect {x = -54, y = -47} {x = -38, y = -31} white ::
          Rect {x = 46, y = -47} {x = 62, y = -31} white ::
          []
      in assertEqual expected result) ::
    []

obstacle : Test
obstacle = test "Obstacle to Rect" <|
  let
    result = rectObstacle testCfg (Obstacle 12 (23, 42))
    expected = Rect {x = 23, y = 12} {x = 42, y = 28} red
  in assertEqual expected result

collision : Test
collision = ElmTest.suite "collision" <|
  (test "detects collisions" <|
    let
      a = Rect {x = 0, y = 0} {x = 10, y = 20} red
      b = Rect {x = 5, y = 18} {x = 50, y = 50} white
    in assert (collides a b)) ::
  (test "detects non-collisions" <|
    let
      a = Rect {x = 0, y = 0} {x = 10, y = 20} red
      b = Rect {x = 11, y = 18} {x = 50, y = 50} white
    in assert (not (collides a b))) ::
  (test "detects more complicated collisions" <|
    let
      a = Rect {x = 1, y = 0} {x = 2, y = 3} red
      b = Rect {x = 0, y = 1} {x = 3, y = 2} white
    in assert (collides a b)) ::
  (test "detects more complicated collisions" <|
    let
      a = Rect {x = 1, y = 1} {x = 2, y = 3} red
      b = Rect {x = 0, y = 0} {x = 2, y = 2} white
    in assert (collides a b)) ::
  []

moveRectTest : Test
moveRectTest = ElmTest.suite "moveRect" <|
  (test "allows to move a Rect on the x axis" <|
    let
      input = Rect {x = 3, y = 0} {x = 10, y = 20} red
      expected = Rect {x = 3.6, y = 0} {x = 10.6, y = 20} red
    in assertEqual expected (moveRect 0.6 input)) ::
  []
