
module KeyboardStateTests exposing (all)

import           ElmTest exposing (..)
import           Keyboard exposing (KeyCode)
import           Set exposing (Set, toList, empty, insert, fromList)

import           KeyboardState exposing (..)

all : Test
all = suite "KeyboardStateTests" <|
  addPressedButton ::
  removeReleasedButton ::
  []

addPressedButton : Test
addPressedButton =
  test "adds a configured button to the state" <|
    let
      result = update (Down 42) Set.empty
    in assertEqualSets (insert 42 empty) result

removeReleasedButton : Test
removeReleasedButton =
  test "remove a configured button from the state" <|
    let
      result = update (Up 42) (insert 42 empty)
    in assertEqualSets empty result

assertEqualSets : Set comparable -> Set comparable -> Assertion
assertEqualSets a b =
  assertEqual (toList a) (toList b)
