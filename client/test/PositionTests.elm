
module PositionTests exposing (all)

import           ElmTest exposing (..)

import           Configuration exposing (..)
import           Position exposing (..)
import           State exposing (..)

main : Program Never
main =
  runSuite all

cfg : Configuration
cfg = {
    gameHeight = 10,
    gameWidth = 10,
    obstacleVelocity = 1,
    padding = 0,
    playerSize = 1,
    levelUpVelocity = 1,
    levelDownVelocity = 1,
    playerInfluences = [0.25, 0.2],
    transitionFunction = \ x -> x
  }

posLevel : Level
posLevel = Level 0.5

transLevel : Level
transLevel = Level 1.5

velLevel : Level
velLevel = Level 2.5

delta : Delta
delta = Delta 0.5

all : Test
all = suite "PositionTests" <|
  (suite "mkInfluences" <|
     test "empty list"
       (assertEqual [] (mkInfluences [])) ::
     test "unto acceleration"
       (assertEqual [Set 1, Next (Set 2), Next (Next (Set 3))] (mkInfluences [1, 2, 3])) ::
     []) ::
  (suite "fillInDerivatives" <|
     test ""
       (assertEqual [5, 4] (fillInDerivatives delta [3, 0] 5)) ::
     []) ::
  (suite "maxLevel" <|
     test ""
       (assertEqual 5 (maxLevel {cfg | playerInfluences = [1, 2, 3]})) ::
     test ""
       (assertEqual 3 (maxLevel {cfg | playerInfluences = [1, 2]})) ::
     []) ::
  (suite "position" <|
     test "don't move"
       (assertUpdatesTo posLevel NoMovement [0] [0]) ::
     test "move right"
       (assertUpdatesTo posLevel Right [0] [2.5]) ::
     test "move left"
       (assertUpdatesTo posLevel Left [0] [-2.5]) ::
     []) ::
  (suite "transition" <|
     test "don't move"
       (assertUpdatesTo transLevel NoMovement [0, 0] [0, 0]) ::
     test "move right"
       (assertUpdatesTo transLevel Right [3, 0] [3.25, 0.5]) ::
     test "move left"
       (assertUpdatesTo transLevel Left [3, 0] [-0.25, -6.5]) ::
     []) ::
  (suite "velocity" <|
    test "don't move"
      (assertUpdatesTo velLevel NoMovement [0, 0] [0, 0]) ::
    test "move right"
      (assertUpdatesTo velLevel Right [3, 0] [4, 2]) ::
    test "move left"
      (assertUpdatesTo velLevel Left [3, 0] [2, -2]) ::
    []) ::
  []

assertUpdatesTo : Level -> Controls
  -> List Float -> List Float -> Assertion
assertUpdatesTo level controls input expected =
  let
    object = {values = input}
    result = updatePosition cfg level controls delta object
  in assertEqual expected result.values
