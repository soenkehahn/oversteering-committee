
module Tests exposing (..)

import           ElmTest exposing (..)

import           KeyboardStateTests
import           PositionTests
import           RectsTests
import           StateTests

main : Program Never
main =
  runSuite tests

tests : Test
tests =
  suite "test suite" <|
    KeyboardStateTests.all ::
    StateTests.all ::
    RectsTests.all ::
    PositionTests.all ::
    []
