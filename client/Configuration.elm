
module Configuration exposing (..)

import           List exposing (length)

type alias Configuration
  = {
    gameWidth : Float,
    gameHeight : Float,
    obstacleVelocity : Float,
    playerSize : Float,
    padding : Float,
    levelUpVelocity : Float,
    levelDownVelocity : Float,
    playerInfluences : List Float,
    transitionFunction : Float -> Float
  }

maxLevel : Configuration -> Float
maxLevel config =
  let
    len = length config.playerInfluences
  in toFloat (len + len - 1)
