
module State exposing (..)

import           Color exposing (..)
import           Debug exposing (..)
import           Keyboard exposing (..)
import           List exposing (length)
import           Random exposing (..)
import           Set exposing (..)

import           Configuration exposing (..)
import           KeyboardState exposing (KeyboardEvent)
import           Position exposing (..)
import           Rects exposing (..)

type alias State = {
    keys : Set KeyCode,
    level : Level,
    position : Object,
    obstacles : List Obstacle,
    gamePhase : Phase
  }

type Phase
  = Playing
  | GameOver
  | Won

rects : Configuration -> State -> List Rect
rects config state =
  (List.map (rectObstacle config) state.obstacles) ++
  rectsPlayer config state

rectsPlayer : Configuration -> State -> List Rect
rectsPlayer config s =
  let
    half = config.playerSize / 2
    position = getPosition s.position
    start = {
        x = position - half,
        y = (-config.gameHeight / 2) + config.padding
      }
    end = {x = start.x + config.playerSize, y = start.y + config.playerSize}
    result = Rect start end white
    twin =
      if position > (config.gameWidth / 2) - (config.playerSize / 2)
        then [moveRect -config.gameWidth result]
      else if position < (-config.gameWidth / 2) + (config.playerSize / 2)
        then [moveRect config.gameWidth result]
      else []
  in result :: twin

init : State
init = State empty (Level 0) Position.init [] Playing

type Message
  = KeyEvent KeyboardEvent
  | Tick Delta
  | NewObstacle
  | AddObstacle Obstacle
  | GCObstacles

  | Cheat (Level -> Level)
  | Noop

update : Configuration -> Message -> State -> (State, Cmd Message)
update config message {keys, level, position, obstacles, gamePhase} =
  let
    mkState = case message of
      KeyEvent e -> State (KeyboardState.update e keys) level position
      Tick delta ->
        let
          left = member 37 keys
          right = member 39 keys
        in if right && left
          then State keys level (updatePosition config level NoMovement delta position)
        else if left
          then State keys level <| updatePosition config level Left delta position
        else if right
          then State keys level <| updatePosition config level Right delta position
        else State keys level (updatePosition config level NoMovement delta position)
      NewObstacle -> State keys level position
      AddObstacle _ -> State keys level position
      GCObstacles -> State keys level position
      Cheat f -> State keys (f level) position
      Noop -> State keys level position
    newGamePhase = addPhase config gamePhase level
    newState =
      first (wrapPosition config) <|
      first (updateLevel config message) <|
      first (\ f -> f newGamePhase) <|
      handleObstacles config obstacles message <|
      mkState
    oldState = State keys level position obstacles gamePhase
  in case newGamePhase of
    Playing -> newState
    GameOver -> {oldState | gamePhase = newGamePhase} ! []
    Won -> {oldState | gamePhase = newGamePhase} ! []

wrapPosition : Configuration -> State -> State
wrapPosition config state =
  let
    new = case state.position.values of
      (position :: r) ->
        wrap (-config.gameWidth / 2, config.gameWidth / 2) position :: r
      [] -> []
  in {state | position = {values = new}}

wrap : (Float, Float) -> Float -> Float
wrap (lower, upper) x =
  let
    delta = upper - lower
  in if x < lower
    then wrap (lower, upper) (x + delta)
  else if x > upper
    then wrap (lower, upper) (x - delta)
  else x

handleObstacles : Configuration -> List Obstacle
  -> Message -> (List Obstacle -> a) -> (a, Cmd Message)
handleObstacles config obstacles message f =
  let
    update (Delta delta) (Obstacle p range) =
      Obstacle (p - delta * config.obstacleVelocity) range
  in case message of
    Tick delta -> (f <| List.map (update delta) obstacles, Cmd.none)
    NewObstacle -> (f obstacles, generate AddObstacle (obstacleGenerator config))
    AddObstacle o -> (f (o :: obstacles), Cmd.none)
    GCObstacles -> (f (List.filter (isAboveLowerEdge config) obstacles), Cmd.none)
    _ -> (f obstacles, Cmd.none)

obstacleGenerator : Configuration -> Generator Obstacle
obstacleGenerator config =
  (andThen (float 100 200) <| \ length ->
   Random.map (\ x -> Obstacle (config.gameHeight / 2) (x, x + length))
     (float (-config.gameWidth / 2) ((config.gameWidth / 2) - length)))

type Obstacle
  = Obstacle Float (Float, Float)

isAboveLowerEdge : Configuration -> Obstacle -> Bool
isAboveLowerEdge config (Obstacle p _) =
  p > (-config.gameHeight / 2) - config.playerSize

rectObstacle : Configuration -> Obstacle -> Rect
rectObstacle config (Obstacle position (from, to)) =
  Rect {x = from, y = position} {x = to, y = position + config.playerSize} red

updateLevel : Configuration -> Message -> State -> State
updateLevel config m s = case m of
  Tick (Delta delta) ->
    let
      player = rectsPlayer config s
      obstacles = List.map (rectObstacle config) s.obstacles
      (Level old) = s.level
      new = Level <|
        if List.any (\ p -> List.any (collides p) obstacles) player
          then old - (delta * config.levelDownVelocity)
        else old + (delta * config.levelUpVelocity)
    in {s | level = new}
  _ -> s

mkCheatMessage : KeyCode -> Message
mkCheatMessage key = case key of
  73 -> Cheat (\ (Level l) -> Level (l - 0.1))
  86 -> Cheat (\ (Level l) -> Level (l + 0.1))
  _ -> Noop

first : (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

addPhase : Configuration -> Phase -> Level -> Phase
addPhase config phase (Level level) = case phase of
  Playing ->
    if level < 0
      then GameOver
    else if level > maxLevel config
      then Won
    else Playing
  Won -> Won
  GameOver -> GameOver
