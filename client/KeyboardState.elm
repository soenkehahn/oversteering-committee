
module KeyboardState exposing (KeyboardEvent(..), update, subscriptions)

import           Keyboard exposing (downs, ups)
import           Set exposing (..)

type alias Key = Int

type KeyboardEvent
  = Down Key
  | Up Key

update : KeyboardEvent -> Set Key -> Set Key
update event state = case event of
  Down key -> insert key state
  Up key -> remove key state

subscriptions : Sub KeyboardEvent
subscriptions = Sub.batch <|
  downs Down ::
  ups Up ::
  []
