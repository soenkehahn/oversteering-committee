
module Rects exposing (..)

import           Collage exposing (..)
import           Color exposing (..)
import           Element exposing (..)
import           Keyboard exposing (..)
import           Set exposing (..)

type alias Rect = {
    from : {x : Float, y : Float},
    to : {x : Float, y : Float},
    color : Color
  }

moveRect : Float -> Rect -> Rect
moveRect f {from, to, color} =
  Rect (movePoint f from) (movePoint f to) color

movePoint : Float -> {x : Float, y : Float} -> {x : Float, y : Float}
movePoint f p = {p | x = p.x + f}

toForms : List Rect -> List Form
toForms = List.map toForm

toForm : Rect -> Form
toForm {from, to, color} =
  let
    width = to.x - from.x
    height = to.y - from.y
  in rect width height
    |> filled color
    |> move (from.x + width / 2, from.y + height / 2)

collides : Rect -> Rect -> Bool
collides a b =
  not <|
    a.to.x < b.from.x ||
    a.from.x > b.to.x ||
    a.to.y < b.from.y ||
    a.from.y > b.to.y
