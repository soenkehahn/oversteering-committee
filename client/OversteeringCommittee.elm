
module OversteeringCommittee exposing (
  ViewState,
  ViewMessage,
  init,
  update,
  subscriptions,
  view)

import           AnimationFrame
import           Collage exposing (..)
import           Color exposing (..)
import           Element exposing (..)
import           Html exposing (Html, div)
import           Html.App exposing (program)
import           Keyboard
import           String
import           Task exposing (perform)
import           Text
import           Time exposing (..)
import           Window exposing (Size)

import           Configuration exposing (..)
import           KeyboardState exposing (KeyboardEvent)
import           Position exposing (Delta(..), Level(..))
import           Rects exposing (..)
import           State exposing (..)

realConfig : Configuration
realConfig = {
    gameWidth = 1000,
    gameHeight = 600,
    obstacleVelocity = 100,
    playerSize = 30,
    padding = 30,
    levelUpVelocity = 0.03,
    levelDownVelocity = 0.45,
    playerInfluences = [0.25, 0.25, 0.25, 0.25, 0.25],
    transitionFunction = \ x -> x ^ 0.05
  }

main : Program Never
main = program {
    init = init,
    update = update,
    subscriptions = subscriptions,
    view = view
  }

-- view

type alias ViewState = {
    size : Size,
    state : State
  }

init : (ViewState, Cmd ViewMessage)
init =
  (ViewState (Size 0 0) State.init,
   Cmd.batch [
     perform (\ _ -> Noop) Resize Window.size,
     perform (\ _ -> Noop) (always (StateMessage NewObstacle)) (Task.succeed ())
   ])

type ViewMessage
  = Noop
  | Resize Size
  | StateMessage Message

update : ViewMessage -> ViewState -> (ViewState, Cmd ViewMessage)
update = updateWithConfig realConfig

updateWithConfig : Configuration -> ViewMessage -> ViewState -> (ViewState, Cmd ViewMessage)
updateWithConfig config message state = case message of
  Noop -> state ! []
  Resize size -> {state | size = size} ! []
  StateMessage m ->
    let (new, cmd) = State.update config m state.state
    in ({state | state = new}, Cmd.map StateMessage cmd)

subscriptions : ViewState -> Sub ViewMessage
subscriptions state = case state.state.gamePhase of
  Playing ->
    Sub.batch <|
      (Sub.map StateMessage <| Sub.batch <|
        Sub.map KeyEvent KeyboardState.subscriptions ::
        AnimationFrame.diffs (Tick << Delta << max 0 << inSeconds) ::
        Keyboard.downs mkCheatMessage ::
        []) ::
      Window.resizes Resize ::
      Time.every second (always (StateMessage NewObstacle)) ::
      Time.every second (always (StateMessage GCObstacles)) ::
      []
  GameOver -> Window.resizes Resize
  Won -> Window.resizes Resize

view : ViewState -> Html m
view = viewWithConfig realConfig

viewWithConfig : Configuration -> ViewState -> Html m
viewWithConfig config state =
  toHtml <|
  container state.size.width state.size.height middle <|
  collage (round config.gameWidth) (round config.gameHeight) <|
    (rect config.gameWidth config.gameHeight |> filled black) ::
    toForms (viewLevel config state.state.level) ++
    toForms (rects config state.state) ++
    viewPhase state.state.gamePhase ++
    viewDebug config state.state ::
    []

viewLevel : Configuration -> Level -> List Rect
viewLevel config (Level level) =
  let barWidth = config.gameWidth / maxLevel config
      inner level offset =
        if level < 1
          then
            let
              start = {x = -config.gameWidth / 2 + offset - 1, y = -config.gameHeight / 2}
              end = {x = start.x + barWidth + 1, y = start.y + (level * config.gameHeight)}
            in [Rect start end yellow]
          else
            let
              start = {x = -config.gameWidth / 2 + offset - 1, y = -config.gameHeight / 2}
              end = {x = start.x + barWidth + 1, y = config.gameHeight / 2}
            in Rect start end yellow :: inner (level - 1) (offset + barWidth)
  in inner level 0

viewPhase : Phase -> List Form
viewPhase phase = case phase of
  Playing -> []
  GameOver -> [viewText "Game\nOver!" red]
  Won -> [viewText "YOU\nWIN!!!" blue]

viewText : String -> Color -> Form
viewText text color =
  Collage.toForm (centered (Text.height 200 (Text.color color (Text.fromString text))))

viewDebug : Configuration -> State -> Form
viewDebug config state =
  let
    t = String.join " " <| List.map showNumber state.position.values
  in viewDebugText config t

showNumber : Float -> String
showNumber x =
  String.right 6 (String.padLeft 6 ' ' (toString (round x)))

viewDebugText : Configuration -> String -> Form
viewDebugText config s =
  let
    t = Text.fromString s
      |> Text.color white
      |> Text.monospace
    (width, height) = sizeOf (centered t)
    movement = (config.gameWidth / 2 - toFloat width / 2, config.gameHeight / 2 - toFloat height / 2)
  in move movement <| Collage.toForm (centered t)
