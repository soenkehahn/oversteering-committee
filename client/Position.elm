
module Position exposing (
  Level(..),
  Delta(..),
  Controls(..),
  Object,
  init,
  getPosition,
  updatePosition,
  fillInDerivatives,
  Influence(..),
  mkInfluences
  )

import           Configuration exposing (..)
import           Debug exposing (..)

type Delta = Delta Float

type Level = Level Float

type Controls
  = NoMovement
  | Left
  | Right

type alias Object = {
    values : List Float
  }

init : Object
init = {values = []}

getPosition : Object -> Float
getPosition object = case object.values of
  p :: _ -> p
  [] -> 0

updatePosition : Configuration -> Level -> Controls -> Delta
  -> Object -> Object
updatePosition config level controls delta {values} =
  let
    initialized = case values of
      [] -> List.map (always 0) config.playerInfluences
      _ -> values
    newPos = newPosition config level controls delta initialized
    new = fillInDerivatives delta initialized newPos
  in {values = new}

newPosition : Configuration -> Level -> Controls -> Delta -> List Float -> Float
newPosition config level controls delta values =
  pickPhase level (phases config) config controls delta values

pickPhase : Level -> List (PhaseRatio -> Phase) -> Phase
pickPhase (Level level) phases = case phases of
  [a] -> a (PhaseRatio level)
  a :: r -> if level < 1
    then a (PhaseRatio level)
    else pickPhase (Level (level - 1)) r
  [] -> crash "out of phases"

type alias Phase =
  Configuration -> Controls -> Delta -> List Float -> Float

type PhaseRatio = PhaseRatio Float

fillInDerivatives : Delta -> List Float -> Float -> List Float
fillInDerivatives (Delta delta) oldList new =
  case oldList of
    old :: r ->
      let
        newDerivative = (new - old) / delta
      in new :: fillInDerivatives (Delta delta) r newDerivative
    [] -> []

-- influence in different phases

type Influence
  = Next Influence
  | Set Float

mapInfluence : (Float -> Float) -> Influence -> Influence
mapInfluence f influence = case influence of
  Next next -> Next (mapInfluence f next)
  Set i -> Set (f i)

purePhase : Influence -> Phase
purePhase rawInfluence config controls delta values =
  let
    scaleControls = case controls of
      NoMovement -> mapInfluence (always 0)
      Right -> \ x -> x
      Left -> mapInfluence negate
    scaleToGameWidth = mapInfluence (\ i -> i * config.gameWidth)
    influence = scaleControls <| scaleToGameWidth rawInfluence
  in evaluateInfluence influence delta values

mergePhase : Phase -> Phase -> PhaseRatio -> Phase
mergePhase a b (PhaseRatio phaseRatio) config controls delta values =
  let
    x = a config controls delta values
    y = b config controls delta values
    ratio = config.transitionFunction phaseRatio
  in x * (1 - ratio) + y * ratio

evaluateInfluence : Influence -> Delta -> List Float -> Float
evaluateInfluence setter (Delta delta) object =
  case (setter, object) of
    (Set f, (_ :: r)) -> f
    (Next next, (old :: r)) -> case evaluateInfluence next (Delta delta) r of
      derivative -> old + derivative * delta
    _ -> crash "not enough values"

-- phases configuration

phases : Configuration -> List (PhaseRatio -> Phase)
phases config = mkPhases (influences config)

mkPhases : List Influence -> List (PhaseRatio -> Phase)
mkPhases influences = case influences of
  (a :: b :: r) ->
    always (purePhase a) ::
    mergePhase (purePhase a) (purePhase b) ::
    mkPhases (b :: r)
  [last] ->
    always (purePhase last) ::
    []
  [] -> []

influences : Configuration -> List Influence
influences config = mkInfluences config.playerInfluences
  -- fixme: don't evaluate every frame

mkInfluences : List Float -> List Influence
mkInfluences list = case list of
  a :: r -> Set a :: List.map Next (mkInfluences r)
  [] -> []
